## How to setup
+ Download 3D Slicer (at least **4.11.0** nightly build version from the official site [https://download.slicer.org/]
+ In the Python Interactor in Slicer, type 
    ```
        pip_install("scipy")
    ``` (For the dynamicThreshold branch, type ```pip_install("natsort")``` in addition to scipy)
+ Add the custom module: 
    + Open slicer
    + Click Edit from the top menu bar
    + Select application setting
    + In the settings window, go to Modules
    + In "Additional Module Path", click add to add the path to the **module** folder (e.g. where/this/module/is/cloned/to/SphericalModel/SphericalModelModule)
    + In the settings window, go to Developer
    + Select the enable developer mode check box
    + Restart slicer when asked 
+ In the module list, search for Spherical Model (Or look for the Approximation tab)

## How to run
+ In the Scan field, select a .nii (Nifti) file of your CT scan.
+ In the Mask field, select a corresponding segmentation mask of the scan that you input above. The mask needs to be a binary mask (only consisting of 0s and 1s)
+ Input a threshold number that will allow you to select your volume of interest. You may consider using segmentation in slicer to assist you with this decision.
+ Click the button to calculate bone parameters

## What to expect
+ The module uses spheres to better estimate the input structure.
+ The module takes in the input and scan to calculate the parameters of interests, such as the mean trabecular thickness, mean trabecular spacing, total trabecular volume, and total volume (trabecula + spacing).
+ The module implements the algorithm stated in the background paper. On a high level, we use a distance field (DF) to calculate mean trabecular thickness using spherical models. This is accomplished by finding the maximum number in the DF repeatedly, until the termination criteria is satisfied. The maximum number in the DF represents the largest sphere that can be fitted in the volume, avoiding the need to grow/absorb regions. Experiments are being conducted currently to determine the best termination criteria.

![This diagram illustrates the general idea](spheres.png)

## Background paper:
+ A new method for the model-independent assessment of thickness in three-dimensional images by T. HILDEBRAND & P. RUEGSEGGER
