import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import logging
import numpy as np
from scipy import ndimage

class SphericalModel(ScriptedLoadableModule):

    def __init__(self, parent):
        ScriptedLoadableModule.__init__(self, parent)
        self.parent.title = "Spherical Model" # TODO make this more human readable by adding spaces
        self.parent.categories = ["Approximation"]
        self.parent.dependencies = []
        self.parent.contributors = ["Tina Wu (Sunnybrook Health Science Centre)"] 
        self.parent.helpText = """
This module approximates trabecular parameters using spherical model described in https://onlinelibrary.wiley.com/doi/abs/10.1046/j.1365-2818.1997.1340694.x
"""
        self.parent.helpText += self.getDefaultModuleDocumentationLink()
        self.parent.acknowledgementText = """

""" # replace with organization, grant and thanks


class SphericalModelWidget(ScriptedLoadableModuleWidget):

    def setup(self, tabLayout = None):
        
        if tabLayout == None:
            ScriptedLoadableModuleWidget.setup(self)
            self.reloadCollapsibleButton.visible = False
            tab = qt.QTabWidget()
            tabLayout = qt.QFormLayout(tab)
            self.layout.addWidget(tab)
            try:
                parent = slicer.util.findChildren(text='Data Probe')[0]
                parent.setChecked(False)
            except IndexError:
                print("No Data Probe frame - cannot collapse")
                return
        self.corticalLabelOn = 1
        self.boneThreshold = 150
        self.roiVolume = None
        self.corticalNode = None
        self.setupDeveloperSection()

        lm = slicer.app.layoutManager()
        redWidget = lm.sliceWidget('Red')
        self.redController = redWidget.sliceController()
        self.redLogic = redWidget.sliceLogic()

        yellowWidget = lm.sliceWidget('Yellow')
        self.yellowController = yellowWidget.sliceController()
        self.yellowLogic = yellowWidget.sliceLogic()

        greenWidget = lm.sliceWidget('Green')
        self.greenController = greenWidget.sliceController()
        self.greenLogic = greenWidget.sliceLogic()
    
        self.filesGroupBox = ctk.ctkCollapsibleGroupBox()
        self.filesGroupBox.title = "Files for Calculation"
        tabLayout.addWidget(self.filesGroupBox)
        self.filesGroupBox.collapsed = False
        self.filesGroupBoxLayout = qt.QFormLayout(self.filesGroupBox)

        self.scanDirSelector = ctk.ctkPathLineEdit()
        #self.fixedVolmDirSelector.filters = ctk.ctkPathLineEdit.Dirs
        self.scanDirSelector.settingKey = 'DICOMPatcherInputDir'
        self.filesGroupBoxLayout.addRow("Scan: ", self.scanDirSelector)

        self.maskDirSelector = ctk.ctkPathLineEdit()
        #self.movingVolmDirSelector.filters = ctk.ctkPathLineEdit.Dirs
        self.maskDirSelector.settingKey = 'DICOMPatcherInputDir'
        self.filesGroupBoxLayout.addRow("Mask: ", self.maskDirSelector)

        self.thresholdInput = qt.QLineEdit("4482")
        self.filesGroupBoxLayout.addRow("Threshold: ", self.thresholdInput)

        self.findmt = qt.QPushButton("Find Mean Thickness")
        self.filesGroupBoxLayout.addRow(self.findmt)
        self.findmt.connect('clicked(bool)', self.findmtSelected)
        self.logic = SphericalModelLogic()

    def findmtSelected(self):
        scanFilePath = self.scanDirSelector.currentPath
        maskFilePath = self.maskDirSelector.currentPath
        imageThreshold = float(self.thresholdInput.text)
        self.logic.run(scanFilePath, maskFilePath, imageThreshold)

class SphericalModelLogic(ScriptedLoadableModuleLogic):
    def __init__(self):
        self.numpyToIjk = np.array([0,0,1,0,0,1,0,0,1,0,0,0,0,0,0,1]).reshape([4,4])
        self.ijkToRas = np.eye(4)

        self.scanVolumeBaseName = ''
        self.maskVolumeBaseName = ''

    def loadVolume(self, scanFilePath, maskFilePath):
        self.scanVolumeBaseName = scanFilePath.split("/")[-1]
        self.scanVolumeBaseName = self.scanVolumeBaseName.split(".")[0] + '_volume'
        scanVolume = slicer.util.loadVolume(scanFilePath, {'name':self.scanVolumeBaseName})
        self.maskVolumeBaseName = maskFilePath.split("/")[-1]
        self.maskVolumeBaseName = self.maskVolumeBaseName.split(".")[0] + '_volume'
        maskVolume = slicer.util.loadVolume(maskFilePath, {'name':self.maskVolumeBaseName})

        return scanVolume, maskVolume
    
    def cropImg(self,img):
        # Copied from https://stackoverflow.com/questions/31400769/bounding-box-of-numpy-array
        r = np.any(img, axis=(1, 2))
        c = np.any(img, axis=(0, 2))
        z = np.any(img, axis=(0, 1))
        rmin, rmax = np.where(r)[0][[0, -1]]
        cmin, cmax = np.where(c)[0][[0, -1]]
        zmin, zmax = np.where(z)[0][[0, -1]]
        return img[rmin:rmax, cmin:cmax, zmin:zmax]

    def checkOrthonormal(self, n4x4matrix, identifier):
        for i in range(3):
            if i < 2:
                result = np.dot(n4x4matrix[:3,i],n4x4matrix[:3,i+1])
            else:
                result = np.dot(n4x4matrix[:3,i], n4x4matrix[:3,0])

            if result > 1e-4:
                print(identifier + 'not ortho @', i)

        for i in range(3):
            result = np.linalg.norm(n4x4matrix[:3,i])

            if int(result) != 1:
                print( identifier + ' not normal @', i)

    def transformUncroppedToRas(self, uncropped_np):
        uncroppedNpToIjk = np.dot(self.numpyToIjk, uncropped_np)
        #self.checkOrthonormal(uncroppedNpToIjk, 'npToUncroppedIjk')
        uncroppedToRas = np.dot(self.ijkToRas, uncroppedNpToIjk)
        #self.checkOrthonormal(uncroppedToRas, 'uncroppedToRas')

        uncroppedToRas_vmat = vtk.vtkMatrix4x4()
        slicer.util.updateVTKMatrixFromArray(uncroppedToRas_vmat, uncroppedToRas)
        uncroppedToRas_transformNode = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLTransformNode', 'uncroppedToRas')
        uncroppedToRas_transformNode.SetMatrixTransformToParent(uncroppedToRas_vmat)
        return uncroppedToRas_transformNode

    def sphericalApprox(self, mask):
        ## generate the signed distance field and the coordinates
        #df_in = ndimage.morphology.distance_transform_edt(mask)
        #df_out = ndimage.morphology.distance_transform_edt(~mask)
        sdf = ndimage.morphology.distance_transform_edt(mask) #df_in - df_out
        shape = np.array(mask.shape)
        coords = np.meshgrid(*[np.arange(shape[0]),np.arange(shape[1]),np.arange(shape[2])], indexing='ij')

        ## repeatedly find the largest circle
        circles = []
        initial_elements = shape.size
        percentage = 1

        while percentage > 0.01:
            # find biggest circle that fits in the boundaries
            max_point = np.unravel_index(np.argmax(sdf), shape)
            max_radius = sdf[max_point]
            circles.append((max_point, max_radius))
            # don't allow new circle centers to lie within existing circles
            df_point_squared = sum([(dim - coord) ** 2 for dim, coord in zip(max_point, coords)])
            sdf[df_point_squared < max_radius ** 2] = 0

            percentage = np.count_nonzero(sdf) / initial_elements
            #print('percentage', percentage)

        meanRadius = np.mean(np.array(circles)[:,1])

        return meanRadius

    def addVolumeFromArray(self, volume, array, name):
        """ inputs:
        ---- volume is a slicer volume where the geometry info is being copied from
        ---- array is a numpy array
        """
        node = slicer.vtkMRMLScalarVolumeNode()
        node.SetOrigin(volume.GetOrigin())
        node.SetSpacing(volume.GetSpacing())
        node.SetName(name)
        slicer.util.updateVolumeFromArray(node, array)
        slicer.mrmlScene.AddNode(node)

        return node

    def estimateBoneParameters(self, roi, imageThreshold):

        roi_np = slicer.util.arrayFromVolume(roi)
        roi_mask = (roi_np > 0)*1
        #roi_np = self.cropImg(roi_np)

        roiVolume = np.count_nonzero(roi_mask)

        trabMask = (roi_np >= imageThreshold)*1
        marrowMask = roi_mask - trabMask

        trabVolume = np.count_nonzero(trabMask)
        
        trabThickness = self.sphericalApprox(trabMask)
        marrowThickness = self.sphericalApprox(marrowMask)

        spacing = roi.GetSpacing()[0]
        trabMaskName = self.scanVolumeBaseName + "_trabMask"
        marrowMaskName = self.scanVolumeBaseName + "_marrowMask"
        trabMaskVolume = self.addVolumeFromArray(roi, trabMask, trabMaskName)
        marrowMaskVolume = self.addVolumeFromArray(roi, marrowMask, marrowMaskName)

        return trabThickness*2*spacing, marrowThickness*2*spacing, trabVolume*spacing, roiVolume*spacing

    def checkVolumes(self, scanVolume, maskVolume):
        
        

        if scanVolume.GetImageData().GetDimensions() != maskVolume.GetImageData().GetDimensions():
            logging.info('shapes of the scan and the mask do not match. use resample tools first')
            return
        if scanVolume.GetSpacing() != maskVolume.GetSpacing():
            logging.info('spacing of the volumes do not match. use upsample tools first')
            return


    def run(self, scanFilePath, maskFilePath, imageThreshold):

        logging.info('Loading volumes')
        scanVolume, maskVolume = self.loadVolume(scanFilePath, maskFilePath)

        self.checkVolumes(scanVolume, maskVolume)

        logging.info('Cropping to region of interest')
        roiName = self.scanVolumeBaseName + 'roiInRas'
        roiInRas = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLScalarVolumeNode', roiName)
        parameters = {'InputVolume':scanVolume.GetID(), 'MaskVolume':maskVolume.GetID(), 'OutputVolume':roiInRas.GetID(), 
                      'Label':1, 'Replace': 0} 
        slicer.cli.run(slicer.modules.maskscalarvolume, None, parameters, wait_for_completion=True)

        meanTrabThickness, meanMarrowThickness, trabVolume, totalVolume = self.estimateBoneParameters(roiInRas, imageThreshold)
        print('mean trabecular thickness: %.4f mm' %(meanTrabThickness))
        print('mean trabecular spacing: %.4f mm' %(meanMarrowThickness))
        print('trabecular volume: %.4f mm3' %(trabVolume))
        print('total volume: %.4f mm3' %(totalVolume))

        return True

